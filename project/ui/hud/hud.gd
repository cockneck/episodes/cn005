class_name HUD
extends Control
# Heads-up display which contains useful information for player.


signal faded_in
signal dialogue_hidden
signal item_selected(id, count)
signal next_passage_requested
signal option_selected(index)
signal phone_hidden

onready var curtain: Curtain = $Curtain
onready var dialogue_overlay: DialogueOverlay = $Overlays/Dialogue
onready var useable_prompt: UseablePrompt = $UseablePrompt


func _ready():
	curtain.fade_out()

	for overlay in $Overlays.get_children():
		overlay.modulate.a = 0
		overlay.visible = false


func dialogue_playback_begin():
	dialogue_overlay.show()


func dialogue_playback_end():
	dialogue_overlay.hide()
	yield(dialogue_overlay, "hidden")
	emit_signal("dialogue_hidden")


func dialogue_show_options(titles):
	dialogue_overlay.set_options(titles)


func dialogue_triggered(speaker: String, text: String):
	dialogue_overlay.triggered(speaker, text)


func set_selected_useable(useable: Useable):
	useable_prompt.set_useable(useable)


func _on_Curtain_faded_out():
	emit_signal("faded_in")


func _on_Dialogue_next_passage_requested():
	emit_signal("next_passage_requested")


func _on_Dialogue_option_selected(index):
	emit_signal("option_selected", index)


func dialogue_playback_ended():
	pass # Replace with function body.
