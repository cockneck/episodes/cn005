class_name DialogueOverlay
extends Overlay
# Class which visually shows the dialogue playing out.


signal next_passage_requested
signal option_selected(index)

const SCROLL_SPEED := 66.6

onready var body_label: Label = $Body/Label
onready var header_label: Label = $Header/Label
onready var options: Array = $Header/Options.get_children()


func _ready():
	for option in options:
		option.connect("pressed", self, "_on_option_selected", [option.get_index()])
		option.visible = false


func _process(delta: float):
	var length = body_label.text.length()

	if length == 0:
		body_label.percent_visible = 1
		return

	body_label.percent_visible += delta * SCROLL_SPEED / length


func set_options(data: Array):
	for option in options:
		var index = option.get_index()

		if index < data.size():
			option.text = data[index].title
			option.visible = true
			continue

		option.visible = false


func show():
	body_label.text = ""
	header_label.text = ""
	.show()


func skip():
	if body_label.percent_visible < 1:
		body_label.percent_visible = 1
		return

	emit_signal("next_passage_requested")


func triggered(speaker: String, text: String):
	body_label.percent_visible = 0
	body_label.text = text
	header_label.text = speaker


func _on_option_selected(index: int):
	emit_signal("option_selected", index)
	set_options([])
