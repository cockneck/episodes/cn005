class_name Overlay
extends ColorRect
# An overlay of the HUD.


signal hidden
signal shown

var _fade_tween: RangeTween = RangeTween.new()


func _ready():
	add_child(_fade_tween)
	_fade_tween.connect("completed", self, "_on_fade_tween_completed")
	_fade_tween.connect("updated", self, "_on_fade_tween_updated")
	_fade_tween.setup(.1, 0.0, 1.0, 0.0)


func hide():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	_fade_tween.fade_to(0.0)


func show():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	_fade_tween.fade_to(1.0)
	.show()


func _on_fade_tween_completed():
	match modulate.a:
		0.0:
			.hide()
			emit_signal("hidden")

		1.0:
			emit_signal("shown")


func _on_fade_tween_updated(value: float):
	modulate.a = value
