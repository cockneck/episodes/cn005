class_name UseablePrompt
extends VBoxContainer


onready var _left_action: UseablePromptAction = $HBoxContainer/LeftAction
onready var _right_action: UseablePromptAction = $HBoxContainer/RightAction
onready var _title: Label = $Title
onready var _tween: RangeTween = $RangeTween


func _ready():
	modulate.a = 0.0


func set_useable(useable: Useable):
	if not useable:
		_tween.fade_to(0.0)
		return

#	_left_action.set_title(useable.get_left_action())
#	_right_action.set_title(useable.get_right_action())
	_title.text = useable.get_title()
	_tween.fade_to(1.0)


func _on_RangeTween_updated(value: float):
	modulate.a = value
