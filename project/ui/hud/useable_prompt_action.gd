class_name UseablePromptAction
extends HBoxContainer
# Shows player that they can use item.


export(bool) var flip_h: bool = false

onready var _title: Label = $Title


func _ready():
	$Icon.flip_h = flip_h


func set_title(text):
	if not text:
		visible = false
		return

	visible = true
	_title.text = text
