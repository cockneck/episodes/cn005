class_name Curtain
extends ColorRect


signal faded_in
signal faded_out

const FADE_TIME = .5

onready var _tween: Tween = Tween.new()


func _ready():
	add_child(_tween)


func fade(to: float):
	_tween.interpolate_property(self, "color:a", color.a, to, FADE_TIME)
	_tween.start()


func fade_in():
	fade(1.0)
	yield(_tween, "tween_completed")
	emit_signal("faded_in")


func fade_out():
	fade(0.0)
	yield(_tween, "tween_completed")
	emit_signal("faded_out")


func pop_in():
	color.a = 1.0
