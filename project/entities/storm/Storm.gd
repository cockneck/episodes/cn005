extends Spatial

export(float) var speed = 6.66
var phase = 0.0

func _process(delta: float):
	phase += delta

	var k := 1
	for child in get_children():
		var variance := 1.0

		if k == 3:
			variance = -variance

		child.rotation_degrees.x = sin(phase / k) * 10 * k * .75 * variance
		child.rotation_degrees.y += speed * delta * k * variance
		k += 2

