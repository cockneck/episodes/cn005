class_name Useable
extends Area
# An entity which can be used by the player.


signal destroyed
signal used
signal yarn_passage_activated(passage)

const BITMASK = {
	"DISABLED": 256,
	"ENABLED": 512,
}

export(bool) var enabled: bool = true setget _set_enabled, _get_enabled
export(String) var title: String = "SET TITLE"


func destroy():
	emit_signal("destroyed")
	queue_free()


func get_title():
	return title


func use():
	emit_signal("used")


func _get_enabled() -> bool:
	return collision_layer == BITMASK.ENABLED


func _set_enabled(enabled: bool):
	var bitmask = BITMASK.ENABLED if enabled else BITMASK.DISABLED
	collision_layer = bitmask
	collision_mask = bitmask
