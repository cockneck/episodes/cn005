class_name HingedDoor
extends Door


export(float) var open_angle: float = 135.0

onready var _default_angle = rotation_degrees.y
onready var _tween: Tween = $Tween


func close():
	_tween_to(_default_angle)
	.close()


func open():
	_tween_to(_default_angle + open_angle)
	.open()


func _tween_to(angle: float):
	_tween.stop_all()
	_tween.interpolate_property(self, "rotation_degrees:y", rotation_degrees.y, angle, .25)
	_tween.start()
