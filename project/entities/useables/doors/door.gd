class_name Door
extends Useable


enum DoorState {
	CLOSED,
	OPEN,
}

export(bool) var is_locked: bool = false
export(DoorState) var state: int = DoorState.CLOSED


func close():
	rotation_degrees.y = 0
	state = DoorState.CLOSED
	title = "Open"


func get_title():
	return ("%s [LOCKED]" % title) if is_locked else title


func open():
	rotation_degrees.y = 135
	state = DoorState.OPEN
	title = "Close"


func use():
	if is_locked:
		return

	match state:
		DoorState.OPEN:
			close()
		DoorState.CLOSED:
			open()

	.use()
