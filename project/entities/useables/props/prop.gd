class_name Prop
extends Useable


export(String) var yarn_passage: String = ""


func use():
	emit_signal("yarn_passage_activated", yarn_passage)
	.use()
