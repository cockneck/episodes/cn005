class_name AnswerScreen
extends Useable


export(String) var answer: String = ""
export(String) var letter: String = ""

onready var _text_plane: TextPlane = $TextPlane


func _ready():
	_update()


func _process(_delta: float):
	_update()


func _update():
	if not _get_enabled():
		_text_plane.text = ""
		return

	_text_plane.text = "%s) %s" % [letter, answer]
