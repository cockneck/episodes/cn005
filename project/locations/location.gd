extends Node
class_name Location


onready var player: PlayerCharacter = $PlayerCharacter

onready var _yarn_player: YarnPlayer = $YarnPlayer


func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)

	for node in $Useables.get_children():
		for child in node.get_children():
			child.connect("yarn_passage_activated", self, "_on_Useable_yarn_passage_activated")


func _on_Useable_yarn_passage_activated(passage: String):
	_yarn_player.play(passage)


func _on_YarnPlayer_playback_began():
	player.is_paused = true


func _on_YarnPlayer_playback_ended():
	yield(get_tree().create_timer(.01), "timeout")
	player.is_paused = false
